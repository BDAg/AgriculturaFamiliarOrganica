export class Usuario{
    constructor(
        public email: string,
        public senha: string,
        public nome: string,
    ) {}
}