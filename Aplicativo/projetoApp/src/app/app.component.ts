import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/login/login";
import {ProductPage} from "../pages/product/product";
import {ProfilePage} from "../pages/profile/profile";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;

  pagesx: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    this.pagesx = [
      { title: 'Home', component: HomePage, icon: 'home'},
      { title: 'Carrinho', component: ProductPage, icon: 'cart' }, // ---> Carrinho
      { title: 'Vender', component: ProductPage, icon: 'pricetags' },         // ---> Meus Produtos
      { title: 'Minhas Vendas', component: ProductPage, icon: 'logo-usd' },  // ---> Minhas Vendas
      { title: 'Meu perfil', component: ProfilePage, icon: 'person' },
      { title: 'Logoff', component: LoginPage, icon: 'arrow-dropleft-circle'}

    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}

