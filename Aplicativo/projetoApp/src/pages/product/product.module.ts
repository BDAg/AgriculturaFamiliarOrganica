import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductPage } from './product';
import {ProductCardPage} from "../product-card/product-card";
import {ProductCardPageModule} from "../product-card/product-card.module";

@NgModule({
  declarations: [
    ProductPage,
    ProductCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductPage),
    ProductCardPageModule
  ],
})
export class ProductPageModule {}
