import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from "../register/register";
import { HomePage } from "../home/home";
import { Usuario } from '../../app/models/usuario.model';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  usuario = {} as Usuario;
  constructor(
    private afAuth: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams) {

  }
  
  emailValido: boolean = false;


  async login(usuario: Usuario) {
    try {
      const result = this.afAuth.auth.signInWithEmailAndPassword(usuario.email, usuario.senha);
      if (result) {
        this.navCtrl.setRoot(HomePage);
      }
    } catch (e) {
      this.emailValido = true;
      console.log('Email ou senha invalido');
    }
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

}
