import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ProductCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-card',
  templateUrl: 'product-card.html',
})
export class ProductCardPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {

  }
  dismiss() {
    this.viewCtrl.dismiss({ test: '1' });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductCardPage');
  }

}
