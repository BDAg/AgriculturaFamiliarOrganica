import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Usuario } from '../../app/models/usuario.model';
import { AngularFireAuth } from 'angularfire2/auth';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  
  usuario = {} as Usuario;

  constructor(
    private afAuth: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }
  
  async register(usuario: Usuario) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(usuario.email, usuario.senha);
      console.log(result);
      console.log("Usuario cadastrado com sucesso!")
    } catch (e) {
      console.error("erro ao cadastrar");
    }
  }
}
